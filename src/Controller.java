public class Controller extends Common {

    public Controller() {
        super();
    }

    private SendThread sThread;
    private ReceiveThread rThread;
    private ElevatorThread[] elevators;
    private int numFloors;
//    private int[][] idlepos;

    public static void main(String[] args) {

        Controller engine = new Controller();
        //TODO arg för antal hissar, våningar, hastighet

        int elevators = 2;
        int floors = 6;

        if (args.length > 0){
            elevators = Integer.parseInt(args[0]);
        }
        if (args.length > 1){
            floors = Integer.parseInt(args[1]);
        }

        engine.init(elevators, floors);
    }

    public void init(int numElevators, int numFloors) {
        sThread = new SendThread(this);
        rThread = new ReceiveThread(this);
        this.numFloors = numFloors;
        elevators = new ElevatorThread[numElevators];
//        idlepos = new int[numElevators][2];

        for (int i = 0; i < elevators.length; i++) {
            elevators[i] = new ElevatorThread(this, numFloors, i + 1, 100, 0);
            elevators[i].start();
        }

        sThread.start();
        rThread.start();

        parse();
    }

    public void parse() {
        int elevID = 0;
        int pFloor = 0;
        int direction = 0;
        while (true) {
            if (this.getParseQueue().peek() != null) {
                String[] input = this.getParseQueue().poll().split(" ");

                elevID = 0;
                pFloor = 0;
                direction = 0;

                switch (input[0]) {
                    case "p": //i hissen
                        elevID = Integer.parseInt(input[1]); //remember!! elevID == elevators[elevID - 1]
                        pFloor = Integer.parseInt(input[2]);
                        if (pFloor == 32000) { //stop button
                            int lastPos = elevators[elevID - 1].getExactPos();
                            elevators[elevID - 1].interrupt();
                            try {
                                elevators[elevID - 1].join();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            try {
                                getSendQueue().put("m " + elevID + " 0");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            elevators[elevID - 1] = new ElevatorThread(this, numFloors, elevID, 100, lastPos);
                            elevators[elevID - 1].start();
                            break;
                        }
                        direction = elevators[elevID - 1].getFloorDirection(pFloor);
                        if (direction == 0){
                            direction = elevators[elevID - 1].getDirection();
                        }
                        elevators[elevID - 1].insertOrder(new Floor(direction, pFloor));
                        break;
                    case "b": //extern knapp
                        pFloor = Integer.parseInt(input[1]);
                        direction = Integer.parseInt(input[2]);
                        int assignedElevator = assign(pFloor, direction);
                        elevators[assignedElevator-1].insertOrder(new Floor(direction, pFloor));
                        break;
                    case "f":
                        elevID = Integer.parseInt(input[1]);
                        elevators[elevID - 1].updateExactPos(Double.parseDouble(input[2]));
                        break;
                    case "v":
                        System.out.print("V ");
                        for (int i = 0; i < elevators.length; i++) {
                            elevators[i].updateSpeed((int) (Double.parseDouble(input[1]) * 100));
                            i++;
                        }
                        break;
                    default:
                        System.out.println("?");
                        break;
                }
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int assign(int floor, int direction) {
        int min = 256000;
        int minID = 0;
        int distanceToNewJob;
        int cost;

        //check distance
        for (ElevatorThread et : elevators) {
            if (direction == et.getDirection()){
                if ((floor*100 - et.getExactPos())*et.getDirection() > 0){
                    distanceToNewJob = Math.abs(et.getExactPos() - floor*100);
                } else {
                    distanceToNewJob = Math.abs(et.getExactPos() - et.getFloorFurthestJob() * 100) + Math.abs((et.getFloorFurthestJob() * 100) - (floor * 100));
                }
            } else {
                distanceToNewJob = Math.abs(et.getExactPos() - et.getFloorFurthestJob() * 100) + Math.abs((et.getFloorFurthestJob() * 100) - (floor * 100));
            }
            cost = distanceToNewJob + ((et.getNumOrdersDown() + et.getNumOrdersUp()) * 100);

            if (cost < min){
                min = cost;
                minID = et.getID();
            }
//            else if (cost == min){
//                minID = et.getID();
//            }
        }
        //TODO some logic here

        System.out.println("Assigning to " + minID);
        return minID; //id of elevator to assign it to
    }
}
