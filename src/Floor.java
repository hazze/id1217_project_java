public class Floor {

    public Floor(int direction, int number){
        this.direction = direction;
        this.number = number;
    }
    public int direction;
    public int number;

    public int compareTo(Floor compareTo){
        return (this.number*this.direction) - (compareTo.number*compareTo.direction);
    }
}
