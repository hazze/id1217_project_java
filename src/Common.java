import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

public class Common {

    private Socket channel;
    private BufferedReader reader;
    private PrintWriter writer;
    private LinkedBlockingQueue<String> sendQueue;
    private LinkedBlockingQueue<String> parseQueue;

    public Common() {
        init();
    }

    public void setSendQueue(LinkedBlockingQueue<String> lbq) {
        this.sendQueue = lbq;
    }

    public LinkedBlockingQueue<String> getSendQueue() {
        return sendQueue;
    }

    public void setParseQueue(LinkedBlockingQueue<String> lbq) {
        this.parseQueue = lbq;
    }

    public LinkedBlockingQueue<String> getParseQueue() {
        return parseQueue;
    }

    public void send(String msg) {
//        System.out.println("Writing " + msg);
        writer.println(msg);
        writer.flush();
    }

    public String receive() {
        String msg = "";
        try {
            msg = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (msg.isEmpty()) {
            return null;
        } else {
            return msg;
        }
    }

    public void init() {
        try {
            channel = new Socket();
            channel.connect(new InetSocketAddress("localhost", 4711));
            reader = new BufferedReader(new InputStreamReader(channel.getInputStream()));
            writer = new PrintWriter(channel.getOutputStream());
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
