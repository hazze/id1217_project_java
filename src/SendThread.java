import java.util.concurrent.LinkedBlockingQueue;

public class SendThread extends Thread {

    private Common engine;
    public LinkedBlockingQueue<String> sendQueue = new LinkedBlockingQueue<>();

    public SendThread (Common engine){
        this.engine = engine;
        this.engine.setSendQueue(sendQueue);
    }

    @Override
    public void run(){
        while (true){
            if (sendQueue.isEmpty()){
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    System.out.println("Sender died");
                    e.printStackTrace();
                }
            } else {
                if (!sendQueue.peek().contains("s")) {
                    System.out.println("Controller: " + sendQueue.peek());
                }
                engine.send(sendQueue.poll());
            }
        }
    }
}
