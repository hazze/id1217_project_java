import java.util.concurrent.LinkedBlockingQueue;

public class ReceiveThread extends Thread {

    private Common engine;
    public LinkedBlockingQueue<String> parseQueue = new LinkedBlockingQueue<>();

    public ReceiveThread(Common engine){
        this.engine = engine;
        this.engine.setParseQueue(parseQueue);
    }

    @Override
    public void run(){
        while (true){
            String msg = engine.receive();
            try {
                parseQueue.put(msg);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("Receiver died");
            }
        }
    }

}

