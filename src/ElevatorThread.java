import java.util.concurrent.atomic.AtomicInteger;

public class ElevatorThread extends Thread {

    private Common engine;
    private Floor[] ordersUp;
    private Floor[] ordersDown;
    private int id;
    private AtomicInteger speed = new AtomicInteger();
    private Floor nextOrder;
    private int direction;
    private AtomicInteger exactPos = new AtomicInteger();
    private int maxFloor;

    public ElevatorThread(Common engine, int maxFloor, int id, int speed, int exactPos) {
        this.engine = engine;
        this.direction = 0;
        this.exactPos.set(exactPos);
        this.ordersDown = new Floor[maxFloor];
        this.ordersUp = new Floor[maxFloor];
        this.id = id;
        this.speed.set(speed);
        this.maxFloor = maxFloor;
    }

    public void updateExactPos(double newPos) {
        int pos = (int) (newPos * 100);
        int estFloor;

        if (pos % 100 > 50) {
            estFloor = (int) Math.ceil(newPos);
        } else {
            estFloor = (int) newPos;
        }

        try {
            engine.getSendQueue().put("s " + id + " " + estFloor);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //System.out.println(id + " pos: " + pos);
        exactPos.set(pos);
    }

    public int getID(){
        return id;
    }

    public int getNumOrdersUp(){
        int num = ((nextOrder == null) ? 0 : 1);
        for (Floor f : ordersUp){
            if (f != null){
                num++;
            }
        }
        return num;
    }

    public int getNumOrdersDown(){
        int num = ((nextOrder == null) ? 0 : 1);
        for (Floor f : ordersDown){
            if (f != null){
                num++;
            }
        }
        return num;
    }

    public int getFloorFurthestJob(){
       if (direction == 1){
           for (int i = ordersUp.length -1; i > 0; i--){
               if (ordersUp[i] != null){
                   return i;
               }
           }
           return nextOrder.number;
       } else if (direction == -1){
           for (int i = 0; i < ordersDown.length; i++){
               if (ordersDown[i] != null){
                   return i;
               }
           }
           return nextOrder.number;
       } else {
           return exactPos.get();
       }
    }

    public int getExactPos() {
        return exactPos.get();
    }

    public int getDirection(){
        return direction;
    }

    public void updateSpeed(int newSpeed) {
        speed.set(newSpeed);
    }

    public int getFloorDirection(int pressedFloor) {
        if (exactPos.get() > pressedFloor * 100) {
            System.out.println("dir -1");
            return -1;
        } else if (exactPos.get() < pressedFloor * 100) {
            System.out.println("dir 1");
            return 1;
        } else {
            System.out.println("dir 0");
            return 0;
        }
    }

    public synchronized void insertOrder(Floor floorder) {
        if (floorder.direction > 0) {
            ordersUp[floorder.number] = floorder;
        } else {
            ordersDown[floorder.number] = floorder;
        }
    }

    private synchronized Floor getOrder() {

        //TODO dont toggle

        if (direction == 1) {
            double pos = exactPos.get() / 100;
            int estFloor = (int) Math.ceil(pos);
            for (int i = estFloor; i < ordersUp.length; i++) {
                if (ordersUp[i] != null) {
                    Floor task = ordersUp[i];
                    ordersUp[i] = null;
                    //System.out.println("1 task: n" + task.number + " d" + task.direction);
                    return task;
                }
            }
        } else if (direction == -1) {
            double pos = exactPos.get() / 100;
            int estFloor = (int) pos;
            for (int i = estFloor; i >= 0; i--) {
                if (ordersDown[i] != null) {
                    Floor task = ordersDown[i];
                    ordersDown[i] = null;
                    //System.out.println("-1 task: n" + task.number + " d" + task.direction);
                    return task;
                }
            }
        } else {
            for (int i = 0; i < ordersUp.length; i++) {
                if (ordersDown[i] != null) {
                    Floor task = ordersDown[i];
                    ordersDown[i] = null;
                    //System.out.println("0 task: n" + task.number + " d" + task.direction);
                    return task;
                }
                if (ordersUp[i] != null) {
                    Floor task = ordersUp[i];
                    ordersUp[i] = null;
                    //System.out.println("0 task: n" + task.number + " d" + task.direction);
                    return task;
                }
            }
        }
        return null;
    }


    public void run() {
        idle();
    }

    private void executeOrder() { //66
        try {
            if (exactPos.get() != nextOrder.number * 100) {
                engine.getSendQueue().put("d " + id + " -1");
                Thread.sleep(2000 * speed.get() / 100);
                engine.getSendQueue().put("m " + id + " " + getFloorDirection(nextOrder.number));
                direction = getFloorDirection(nextOrder.number);
                Floor newOrder = getOrder();
                while (exactPos.get() != nextOrder.number * 100) {
                    //edge case!! :D:D:D:D:D:D
                    if ((nextOrder.number == 0 && (exactPos.get() < 5))) {
                        break;
                    } else if (nextOrder.number == maxFloor - 1 && exactPos.get() > (((maxFloor -1) * 100) - 5)) {
                        break;
                    }
                    //System.out.println("ePos: " + exactPos.get());
                    if (newOrder != null && newOrder.number != nextOrder.number) {
                        insertOrder(new Floor(nextOrder.direction, nextOrder.number));
                        nextOrder = newOrder;
                    } else {
                        Thread.sleep(1);
                        newOrder = getOrder();
                    }
                }
                if (newOrder != null) {
                    insertOrder(newOrder);
                }
                engine.getSendQueue().put("m " + id + " 0");
                Thread.sleep(1000 * speed.get() / 100);
                engine.getSendQueue().put("d " + id + " 1");
//                for (Floor floor : ordersUp) {
//                    if (floor != null) {
//                        System.out.println("qu: n" + floor.number + " d" + floor.direction);
//                    } else {
//                        System.out.println("qu: null");
//                    }
//                }
//                for (Floor floor : ordersDown) {
//                    if (floor != null) {
//                        System.out.println("qd: n" + floor.number + " d" + floor.direction);
//                    } else {
//                        System.out.println("qd: null");
//                    }
//                }
                Thread.sleep(5000 * speed.get() / 100);
                idle();
            } else {
                engine.getSendQueue().put("d " + id + " 1");
                idle();
            }
        } catch (InterruptedException e) {
            System.out.println(id + ": returning");
            return;
        }
    }

    private void idle() {
        nextOrder = getOrder();
        while (nextOrder == null) {
            direction = 0;
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                System.out.println(id + ": returning");
                return;
            }
            nextOrder = getOrder();
        }
        executeOrder();
    }
}
